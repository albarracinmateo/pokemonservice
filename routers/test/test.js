var express = require('express');
var router = express.Router({ mergeParams: true });
var path = '/test/';
const request = require('request');
const pgDatabase = require('../../DB');

function getPokemon(pokemon) {
  let getPoke = {
    'method': 'GET',
    'url': 'https://pokeapi.co/api/v2/pokemon/' + pokemon.toLowerCase(),
    'headers': {
      'Content-Type': 'application/json'
    },
    json: true
  };
  return getPoke;
}

async function executeQuery(queryDB, reqQuery) {
  let response;
  await pgDatabase.query(queryDB, reqQuery).then(async data => {
    console.log("DATA: ", data);
    if (data.length == 0 || data.length > 1) {
      response = data;
    } else {
      response = data[0];
    }
  });
  return response;
}

function validarAtaque(ataque, defensa) {
  if (ataque > defensa) {
    return true;
  }
  return false;
}

function getPosibilidades(ataca, defiende) {
  let total = defiende.defense
  let posibiliad = Math.floor((ataca.attack * 100) / total)
  return posibiliad
}

function ataqueDef(atacante, defensor) {

  let ataque = validarAtaque(atacante.attack, defensor.defense);
  console.log("\n" + atacante.name + " esta atacando!")
  if (ataque) {
    let damage = atacante.attack - defensor.defense;
    let ran = Math.floor(Math.random() * (damage - 1) + 1);
    console.log("Ataque: " + ran);
    defensor.hp = defensor.hp - ran
    console.log("Vida de " + defensor.name + ": " + defensor.hp + '\n');

  } else {
    let posibilidad = getPosibilidades(atacante, defensor)
    let ran2 = Math.floor(Math.random() * (100 - 1) + 1);
    if (ran2 < posibilidad) {
      console.log("Entro al ataque con " + ran2 + " entre " + posibilidad);
      let damage = Math.abs(defensor.attack - atacante.defense);
      let realDamage = damage * 1.2
      let ran = Math.floor(Math.random() * (realDamage - 1) + 1);
      if (ran > atacante.attack) {
        ran = atacante.attack
      }
      console.log("Ataque: " + ran);
      defensor.hp = defensor.hp - ran
      console.log("Vida de " + defensor.name + ": " + defensor.hp + '\n');
    } else {
      console.log("No entro a la pelea");
    }
  }

  return defensor.hp

}

function pelea(primero, segundo) {
  let continua = true
  console.log(segundo);

  while (continua) {
    segundo.hp = ataqueDef(primero, segundo);
    if (segundo.hp <= 0) {
      continua = false;
      console.log(segundo.name + " FUE DERROTADO");

      let resultado = {
        winner: primero,
        loser: segundo
      }

      return resultado;
    }

    primero.hp = ataqueDef(segundo, primero);
    if (primero.hp <= 0) {
      console.log(primero.name + " FUE DERROTADO");
      continua = false;

      let resultado = {
        winner: segundo,
        loser: primero
      }

      return resultado;
    }
  }
}


router.get(path, async function (req, res) {

  console.log("\n-----CONSULTANDO test.GET-----\n");

  if (req.query.pokemon == undefined) {
    return res.status(400).json({ error: "property_pokemon_not_found" })
  }

  let pokemon = req.query.pokemon.toUpperCase();
  console.log("\nPokemon a buscar: ", pokemon);

  const poke = getPokemon(pokemon);
  let selectedPokemon;
  let selectPokemonQuery;

  if (pokemon.length != 0) {
    selectPokemonQuery = "SELECT * FROM pokefight WHERE name = '" + pokemon + "'";
  } else {
    selectPokemonQuery = "SELECT * FROM pokefight";
  }

  console.log("\n" + selectPokemonQuery);
  selectedPokemon = await executeQuery(selectPokemonQuery, req.query);
  console.log("\nPokemon buscado: ", selectedPokemon);

  if (selectedPokemon.length == 0) {
    request(poke, async function (err, response) {

      console.log(poke);
      // console.log("\n", response.body);

      if (response.body.id == undefined) {
        return res.status(400).send({ error: "pokemon_not_found" });
      }

      let insertPokemonQuery = "INSERT INTO pokefight (id, name, hp, attack, defense, velocity) VALUES(" + response.body.id + ", '" + response.body.name.toUpperCase() + "', " + response.body.stats[0].base_stat + ", " + response.body.stats[1].base_stat + ", " + response.body.stats[2].base_stat + ", " + response.body.stats[5].base_stat + ")";
      console.log("\n" + insertPokemonQuery);
      await executeQuery(insertPokemonQuery, req.query);
      return res.status(200).json({ id: response.body.id, name: response.body.name.toUpperCase(), hp: response.body.stats[0].base_stat, attack: response.body.stats[1].base_stat, defense: response.body.stats[2].base_stat, velocity: response.body.stats[5].base_stat });

    })
  } else {
    return res.status(200).json(selectedPokemon);
  }


});

router.post(path, async function (req, res) {

  console.log("\n-----CONSULTANDO test.POST-----\n");

  if (req.query.pokemon1 == undefined || req.query.pokemon2 == undefined) {
    return res.status(400).json({ error: "property_pokemon1_or_pokemon2_not_found" })
  }

  let winnerLoser;

  let pokemon1 = req.query.pokemon1.toUpperCase();
  let pokemon2 = req.query.pokemon2.toUpperCase();

  let selectPokemonQuery1 = "SELECT * FROM pokefight WHERE name = '" + pokemon1 + "'";
  let selectPokemonQuery2 = "SELECT * FROM pokefight WHERE name = '" + pokemon2 + "'";
  console.log("\n" + selectPokemonQuery1);
  console.log("\n" + selectPokemonQuery2);
  selectedPokemon1 = await executeQuery(selectPokemonQuery1, req.query);
  selectedPokemon2 = await executeQuery(selectPokemonQuery2, req.query);


  if (selectedPokemon1.length == 0 || selectedPokemon2.length == 0) {
    if (selectedPokemon1.length == 0) {
      return res.status(400).json({ error: pokemon1 + '_not_found' });
    }
    return res.status(400).json({ error: pokemon2 + '_not_found' });
  }

  if (selectedPokemon1.velocity > selectedPokemon2.velocity) {
    winnerLoser = pelea(selectedPokemon1, selectedPokemon2);
    console.log("\nGANADOR: ", winnerLoser.winner.name);
  }
  else {
    winnerLoser = pelea(selectedPokemon2, selectedPokemon1);
    console.log("\nGANADOR: ", winnerLoser.winner.name);
  }

  let victorys = winnerLoser.winner.victorys + 1;

  let updateWinQuery = "UPDATE pokefight SET victorys = " + victorys + " WHERE id = " + winnerLoser.winner.id;
  console.log("\n" + updateWinQuery);
  await executeQuery(updateWinQuery, req.query);

  let loses = winnerLoser.loser.loses + 1;

  let updateLoseQuery = "UPDATE pokefight SET loses = " + loses + " WHERE id = " + winnerLoser.loser.id;
  console.log("\n" + updateLoseQuery);
  await executeQuery(updateLoseQuery, req.query);

  return res.status(200).json({ winner: winnerLoser.winner.name })
});

module.exports = router;