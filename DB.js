var fs = require("fs");
var promise = require('bluebird');
var pgp = require('pg-promise')({ promiseLib: promise });
var pathConf = require('path');

var devMode = false;
var workPath = process.cwd();
process.argv.forEach(function (val, index, array) {
  if (val == '--dev')
    devMode = true;
  if (array.length > 2 && index == array.length - 1 && !val.includes('--'))
    workPath = val;
});

if (devMode) {
  workPath = __dirname;
  process.cwd = function () {
    return __dirname;
  }
} else {
  console.debug = function (message) {
    process.stdout.write(message);
  }
}

var confFilePath = pathConf.join(workPath, 'server-config.json');
var conf = JSON.parse(fs.readFileSync(confFilePath, 'utf-8'));

//PostgreSQL

var pgConnectionData = {
  host: conf.database.host,
  port: conf.database.port,
  database: conf.database.database,
  user: conf.database.user,
  password: conf.database.password
}

console.log(pgConnectionData);
var pgDatabase = pgp(pgConnectionData);


module.exports = pgDatabase;