var express = require('express');
var http = require('http');
var app = express();

var fs = require('fs');
var bodyParser = require('body-parser');
var cors = require('cors');
var path = require('path');
var devMode = false;
var workPath = process.cwd();

var confFilePath = path.join(workPath, 'server-config.json');
var conf = JSON.parse(fs.readFileSync(confFilePath, 'utf-8'));
var port = conf.port;
var dbFilePath = path.join(workPath, 'DB.sql');
const pgDatabase = require('./DB');

var test = require('./routers/test/test');

process.argv.forEach(function (val, index, array) {
  if (val == '--dev')
    devMode = true;
  if (array.length > 2 && index == array.length - 1 && !val.includes('--'))
    workPath = val;
});

if (devMode) {
  workPath = __dirname;
  process.cwd = function () {
    return __dirname;
  }
} else {
  console.debug = function (message) {
    process.stdout.write(message);
  }
}

app.set('port', port);
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cors());
app.use(bodyParser.json());
app.use(test);

var dataBase = fs.readFileSync(dbFilePath).toString();
pgDatabase.query(dataBase, function (err, result) {
  done();
  if (err) {
    console.log('error: ', err);
    process.exit(1);
  }
  process.exit(0);
});

function startHttpServer() {
  var server = http.createServer(app);
  server.listen(port, function () {
    console.log('\nEl servidor esta corriendo en el puerto: ' + port + "\n");
  });
  return server;
};


express.response.badRequest = function (args) {
  this.writeContinue();
  this.statusCode = 400;
  this.send(args);
  this.end();
};

express.response.sqlError = function (args) {
  this.writeContinue();
  this.statusCode = 503;
  this.send(args);
  this.end();
};

httpServer = startHttpServer();